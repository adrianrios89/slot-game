# Casumo Javascript Game Challenge

## Setup & Run


### 1. Install node.js and npm:

https://nodejs.org/en/


### 2. Install dependencies:

Navigate to the repo's directory.

Run:

```npm install```


### 3. Run the development server:

Run:

```npm run dev```

This will run a server so you can run the game in a browser. Open your browser and enter http://localhost:3000 into the address bar.

## Comments of the challenge

### Ecosystem
The Framework used to develop the game is [**Phaser.io**](https://phaser.io/).

Also the module bundle [**Webpack**](https://webpack.github.io/) and **ES6** are used in the challenge.

### Architecture
The architecture chosen is an unidirectonial MVC (the idea is to approach Flux, an architecture that I really like and that gives such good results). The responsabilties are:

- Views: Launch events when there are user inputs or something has happen (e.g: end of an animation). At the same time views are connected to the **Model** and listening their changes.
- Controllers: Listen **View** events and call **Model** methods in consecuence.
- Models: Are the data owners. The **Model** gets, modifies and deletes the data and notifies that. **Views** are listening these changes.

### Next Steps

- Add Unit Test
- Delete setTimeouts and chain rails animations
- Move magic numbers to config
