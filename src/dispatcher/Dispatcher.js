class Dispatcher {
  init(game, events) {
    this.game = game;
    this.game.events = {};

    events.map((event) => {
        this.game.events[event] = new Phaser.Signal();
    }, this);
  }

  dispatch(event){
    if (this.game.events[event]) {
        const args = [].slice.call(arguments, 1, arguments.length);
        this.game.events[event].dispatch.apply(this.game.events[event], args);
    } else {
        console.log(`EVENT ${event} DOESN\'T EXISTS`);
    }
  }

  listen(ctx, events){
    for (let i = 0, eventsL = events.length; i < eventsL; i++) {
        events.map((event) => {
            const key = Object.getOwnPropertyNames(event)[0],
                item = event[key];
            if (typeof item === 'function') {
                if (this.game.events[key]) {
                    this.game.events[key].add(item, ctx);
                } else {
                    console.log(`EVENT ${key} NOT FOUND`);
                }
            }
        }, this);
    }
  }
}

export default new Dispatcher();
