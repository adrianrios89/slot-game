export default {
  //Game General Config
  gameWidth: 920,
  gameHeight: 600,
  localStorageName: 'phaseres6webpack',
  // Game events
  events: [
    'view.btnlick',
    'view.stopClick',
    'model.newGame',
    'model.stopGame',
  ],
  // Rails
  railsNumber: 5,
  rowsNumber: 3,
  rowsLayouting: {
    x: 79.5,
    y: 139.5,
    spaceBetween: 139
  },
  railsLayouting: {
    rail: {
      x: 55,
      y: 0,
      spaceBetween: 163
    },
    mask: {
      y: 78,
      width: 158,
      height: 410,
      color: 0xffffff
    },
    blurConfig: {
      image: {
        width: 158,
        height: 1741
      },
      tween: {
        y: 50,
        duration: 500
      }
    }
  }
}
