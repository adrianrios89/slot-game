import Phaser from 'phaser';
import config from '../../config';

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset }) {
    super(game, x, y, asset);
    this.init();
  }

  init() {
    //Blur
    const blurConfig = config.railsLayouting.blurConfig;

    this.visible = false;
    this.width = blurConfig.image.width;
    this.height = blurConfig.image.height;

    // Tween
    this.spinTween = game.add.tween(this).to( { y: blurConfig.tween.y }, blurConfig.tween.duration, null, false, 0, -1);

    // Audio
    this.spinAudio = game.add.audio('spin', 1 , true);
  }

  onNewGame(){
    this.visible = true;
    this.spinAudio.play();
    this.spinTween.isPaused && this.spinTween.resume() || this.spinTween.start();
  }

  onStopGame(){
    this.visible = false;
    this.spinAudio.stop();
    this.spinTween.pause();
  }
}
