import Phaser from 'phaser';
import config from '../../config';

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset, figures, railData }) {
    super(game, x, y, asset);
    this.railData = railData;
    this.figures = [];
    this.tweens = [];

    this.init(figures);
  }

  init(figures) {
    for (var i = 0; i < config.rowsNumber; i++) {
      const layout = config.rowsLayouting;

      const figure = this.game.add.sprite(layout.x, layout.y + (layout.spaceBetween * i), 'figures', this.railData[i]);
      figure.anchor.setTo(0.5, 0.5)
      this.figures.push(figure);

      const tween = this.game.add.tween(figure.scale).to({ x: 1.2, y: 1.2}, 200, null, false, 0, 1, true);
      this.tweens.push(tween);

      this.addChild(figure);
    }
  }

  onNewGame(railData) {
    this.visible = false;
    for (var i = 0; i < this.figures.length; i++) {
      const figure = this.figures[i];
      figure.frame = railData[i];
    }
  }

  onStopGame(){
    this.visible = true;
    this.y -= 50;

    // Tween
    this.bounceTween = game.add.tween(this).to( { y: 0 }, 200);
    this.bounceTween.start();
  }

  onPrize(prizes){
    for (var i = 0; i < this.figures.length; i++) {
      const figure = this.figures[i];
      const tween = this.tweens[i];
      if (figure.frame === prizes.figureAwarded ) {
        tween.start();
      }
    }
  }
}
