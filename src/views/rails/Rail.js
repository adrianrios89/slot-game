import Phaser from 'phaser';
import config from '../../config';
import BlurRails from './BlurRail';
import FiguresRails from './FiguresRail';


export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset, railData }) {
    super(game, x, y, asset);

    this.initFigures(railData);
    this.initRailMask();
    this.initBlur();

  }

  initFigures(railData){
    this.figureRail = new FiguresRails({game: this.game, x: 0, y: 0, railData: railData});
    this.addChild(this.figureRail);
  }

  initRailMask(){
    const mask = game.add.graphics(0, 0);
    mask.beginFill(config.railsLayouting.mask.color);
    mask.drawRect(this.x, config.railsLayouting.mask.y, config.railsLayouting.mask.width, config.railsLayouting.mask.height);
    this.mask = mask;
  }

  initBlur() {
    this.blurRail = new BlurRails({game: this.game, x: 0, y: -1250, asset: 'blurFigures'});
    this.addChild(this.blurRail);
  }

  onNewGame(railData){
    this.figureRail.onNewGame(railData);
    this.blurRail.onNewGame();
  }

  onStopGame(){
    this.figureRail.onStopGame();
    this.blurRail.onStopGame();
  }

  onPrize(prizes){
    this.figureRail.onPrize(prizes);
  }
}
