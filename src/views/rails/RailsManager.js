import Phaser from 'phaser'
import Rail from './Rail';
import config from '../../config';
import model from '../../model/Model';
import dispatcher from '../../dispatcher/Dispatcher';


export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset, railData }) {
    super(game, x, y, asset);
    this.rails = [];
    this.winAudio = game.add.audio('win', 4);

    this.initListeners();
    this.createRails();
  }

  initListeners() {
    const events = [
      {'model.newGame': this.onNewGame},
      {'model.stopGame': this.onStopGame},
    ];

    dispatcher.listen(this, events);
  }


  createRails() {
    for (let i = 0; i < config.railsNumber; i++) {
      const railConfig = config.railsLayouting.rail;
      const x = railConfig.x + (railConfig.spaceBetween * i);
      const rail = new Rail({game: this.game, x, y: 0, railData: model.rails[i] });
      this.rails.push(rail);
      this.addChild(rail);
    }
  }

  onNewGame(){
    for (let i = 0; i < this.rails.length; i++) {
      const rail = this.rails[i];
      const delay = (150 * i);
      this.executeNewGameActionsOnRail(rail, delay, i)
    }
  }

  executeNewGameActionsOnRail(rail, delay, i) {
    window.setTimeout((() => rail.onNewGame(model.rails[i])), delay);
  }

  onStopGame(){
    const prizeDelay = 620;
    for (let i = 0; i < this.rails.length; i++) {
      const rail = this.rails[i];
      const delay = (150 * i);
      model.prizes && window.setTimeout(( () => {
        this.winAudio.play();
        rail.onPrize(model.prizes)
      }).bind(rail), prizeDelay);
      window.setTimeout((rail.onStopGame).bind(rail), delay);
    }
  }
}
