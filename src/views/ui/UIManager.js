import Phaser from 'phaser';
import config from '../../config';
import dispatcher from '../../dispatcher/Dispatcher';


export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset, railData }) {
    super(game, x, y, asset);

    this.initListeners();
    this.initButtons();
  }

  initListeners() {
    const events = [
      {'model.newGame': this.onNewGame},
      {'model.stopGame': this.onStopGame},
    ];

    dispatcher.listen(this, events);
  }

  initButtons() {
    this.button = game.add.button(600, 500, 'ui', () => {}, this);
    this.button.onInputDown.add(this.onBtnDown, this);
    this.addChild(this.button);
  }

  onBtnDown() {
    dispatcher.dispatch('view.btnlick');
  }

  onNewGame(){
    this.button.frame = 1;
  }

  onStopGame(){
    this.button.frame = 0;
  }
}
