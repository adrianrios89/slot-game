import Phaser from 'phaser'
import UIManager from './ui/UIManager';
import RailsManager from './rails/RailsManager';

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset }) {
    super(game, x, y, asset);

    const music = game.add.audio('loop', 0.5 , true);
    music.play();

    this.init();
  }

  init() {
    this.width = this.game.world.width;
    this.height = this.game.world.height;

    const railsManager = new RailsManager({game: this.game, x: 0, y: 0 });
    const uiManager = new UIManager({game: this.game, x: 0, y: 0 });

    this.addChild(railsManager);
    this.addChild(uiManager);
  }
}
