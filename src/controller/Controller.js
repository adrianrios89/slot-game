import dispatcher from '../dispatcher/Dispatcher';
import model from '../model/Model';

class Controller {
  init() {
    this.initListeners();
  }

  initListeners() {
    const events = [
      {'view.btnlick': this.onPlayClick},
    ];

    dispatcher.listen(this, events);
  }

  onPlayClick() {
    if (model.isGameInCouse) {
      model.stopGameInCourse();
    } else {
      model.newGameRequest();
    }
  }
}

export default new Controller();
