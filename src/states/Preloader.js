import Phaser from 'phaser';
import { centerGameObjects } from '../utils';

export default class extends Phaser.State {
  init () {}

  preload () {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg');
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar');
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.load.setPreloadSprite(this.loaderBar);

    this.load.image('background', 'assets/images/background.jpg')
    this.load.image('blurFigures', 'assets/images/blurFigures.png');

    game.load.spritesheet('figures', 'assets/images/figures.png', 159, 139);
    game.load.spritesheet('ui', 'assets/images/ui.png', 308, 112);

    game.load.audio('win', ['assets/audio/win.wav']);
    game.load.audio('loop', ['assets/audio/loop.mp3']);
    game.load.audio('spin', ['assets/audio/spin.wav']);
  }

  create () {
    this.state.start('Game');
  }
}
