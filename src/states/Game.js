import Phaser from 'phaser';
import World from '../views/World';
import dispatcher from '../dispatcher/Dispatcher';
import controller from '../controller/Controller';
import model from '../model/Model';
import config from '../config';


export default class extends Phaser.State {
  create () {
    // INIT DISPATCHER
    dispatcher.init(this.game, config.events);
    // INIT CONTROLLER
    controller.init();
    // INIT MODEL
    model.init();
    // INIT VIEWS
    const world = new World({game: this.game, x: 0, y:0, asset: 'background' });
    this.game.add.existing(world);
  }

}
