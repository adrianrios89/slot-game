import dispatcher from '../dispatcher/Dispatcher';

// TODO This should be replaced by a Backend
const randomGames = [
  [[1, 13, 5], [10, 13, 6], [3, 13, 7], [5, 14, 2], [2, 6, 1]], //13
  [[6, 7, 2], [10, 10, 7], [6, 14, 2], [3, 2, 13], [10, 7, 1]],
  [[3, 2, 13], [2, 3, 14], [10, 2, 6], [2, 14, 14], [6, 2, 6]], //2
  [[5, 14, 10], [2, 5, 10], [3, 2, 10], [2, 7, 10], [7, 13, 10]], //10
];

const randomPrizes = [
  { figureAwarded: 13 },
  null,
  { figureAwarded: 2 },
  { figureAwarded: 10 }
];

class Model {
  init() {
    this.isGameInCouse = false;
    this.rails = randomGames[1];
    this.prizes = [];
  }

  newGameRequest(){
    const newGame = Math.floor(Math.random() * 4);
    this.isGameInCouse = true;
    this.rails = randomGames[newGame];
    this.prizes = randomPrizes[newGame];
    dispatcher.dispatch('model.newGame');
  }

  stopGameInCourse() {
    this.isGameInCouse = false;
    dispatcher.dispatch('model.stopGame');
  }
}

export default new Model();
